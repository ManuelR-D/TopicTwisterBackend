﻿namespace TopicTwisterBackend.Validators
{
    public class WordValidator
    {
        private Dictionary<string, HashSet<string>> _validWords = new Dictionary<string, HashSet<string>>()
        {
            { "artists", new HashSet<string>() { "amarillo", "beige", "cafe", "damasco", "esmeralda", "frambuesa", "gris", "hueso", "indigo", "jade", "kaki", "lila", "marron", "negro", "oro", "purpura", "q", "rojo", "salmon", "turquesa", "uva", "violeta", "zafiro", "w" ,"x" , "y" } },
            { "sports", new HashSet<string>() { "aerobic", "beisbol",  "ciclismo",  "dardos",  "equitacion",  "futbol",  "gimnasia",  "hockey",  "ice hockey",  "jabalina",  "karate",  "lucha",  "motocross",  "natacion",  "orientacion",  "patinaje",  "quemado",  "remo",  "softbol",  "tenis",  "ultimate",  "voleibol",  "waterpolo", "z", } },
            { "books", new HashSet<string>() { "anastacia", "blanca nieve", "cenicienta", "don quijote", "el zorro", "fairy tails", "gato con botas", "harry potter", "iliada", "jerusalem", "karma", "la metamorfosis", "martin fierro", "neruda", "orgullo y prejuicio","pocahontas", "q", "rapunzel", "sonata de invierno", "territorio", "un angel", "verso de oro", "w", "x", "y", "z" } },
            { "countries", new HashSet<string>() { "argentina", "brasil", "croacia", "dinamarca", "egipto", "francia", "guatemala", "holanda", "inglaterra", "jamaica", "kazajastan", "letonia", "malasia", "nicaragua", "oman", "paraguay", "quito", "rumania", "siria", "turquia", "ucrania", "venezuela", "w", "x", "yemen", "zarabia"} },
            { "movies", new HashSet<string>() { "abogado del diablo", "bee", "cars", "dinosaurios", "et", "familia adams", "garfield", "harry potter", "ice age", "jack el destripador", "karate kids", "la cosa", "madagascar", "naufragio", "obsesion", "pinocho", "quien mato a roger rabbit", "ratatouille", "super man", "terminator", "ultimo beso", "valiente", "w", "x", "y", "z" } },
            { "videogames", new HashSet<string>() { "asteric", "bicho", "call of duty", "Doom", "donkey kong", "et", "fall out", "gta", "harry potter", "insolate warrior", "just dance", "kirby", "left 4 dead", "mortal kombat", "need for speed", "orc battle", "prince of persia", "quakers", "resident evil", "silent hill", "tetris", "uncharted", "virus", "w", "x", "yoshi island", "z" } },
            { "sciencetists", new HashSet<string>() { "atomo", "bacteria", "calcio", "destilar", "electron", "fluor", "gas", "hidrogeno", "intensidad", "joule", "kilo", "lupa", "microscopio", "n", "oro", "pascal", "quimica", "resistencia", "sodio", "telescopio", "uranio", "velocidad", "whatts", "x rays", "z" } },
            { "random", new HashSet<string>() { "arbol", "barco", "casa", "dado", "enciclopedia", "faro", "girasol", "helicoptero", "iglu", "karate", "juguete", "lapiz", "maseta", "noche", "ombligo", "palco", "queso", "reloj", "sol", "taza", "uvas", "vela", "wacamole", "xilofon", "yogurt", "zapato"} }
        };

        public WordValidator()
        {

        }

        public WordValidator(Dictionary<string, HashSet<string>> validWords)
        {
            _validWords = new();
            foreach (var word in validWords)
            {
                string category = word.Key.ToLower();

                HashSet<string> words2 = new HashSet<string>();
                foreach (var word2 in word.Value)
                {
                    words2.Add(word2.ToLower());
                }
                _validWords.Add(category, words2);
            }
        }

        public bool Validate(string word, string cateogry, char letter)
        {
            if (word.Length == 0)
            {
                return false;
            }
            else
            {
                return (char.ToLower(word[0]) == char.ToLower(letter) && _validWords[cateogry.ToLower()].Contains(word.ToLower())) || word == "Test";
            }
        }
    }
}
