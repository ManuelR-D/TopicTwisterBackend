﻿using TopicTwisterBackend.Controllers;
using TopicTwisterBackend.Model.ModelDAO;
using TopicTwisterBackend.Model.ModelDAO.Interfaces;
using TopicTwisterBackend.Model.ModelDTO;
using TopicTwisterBackend.Service;

namespace TopicTwisterBackend.Generators
{
    public class RandomCategory
    {
        private List<CategoryDTO> _categoriesModel;
        private static List<CategoryDTO> _randomizedCategoriesModel = new();

        public RandomCategory(List<CategoryDTO> categories)
        {
            _categoriesModel = categories;
        }
        public RandomCategory()
        {
            _categoriesModel = new CategoryService(new CategoryMicrosoftSQLDAO()).GetAllCategories();
        }

        public List<CategoryDTO> GetRandomCategories(int amount)
        {
            List<CategoryDTO> shuffledCategories = new();
            Random rnd = new Random();
            for (int i = 0; i < amount; i++)
            {
                int randomIdx = rnd.Next(0, _categoriesModel.Count);
                shuffledCategories.Add(_categoriesModel[randomIdx]);
                _categoriesModel.RemoveAt(randomIdx);
            }
            _categoriesModel.AddRange(_randomizedCategoriesModel);
            return shuffledCategories;
        }
    }
}
