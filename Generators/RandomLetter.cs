﻿using TopicTwisterBackend.Model.ModelDTO;

namespace TopicTwisterBackend.Generators
{
    public class RandomLetter
    {
        private static string _availableLetters = "abcdefghijklmnñopqrstuvwxyz";
        public RandomLetter()
        {

        }

        public RandomLetter(string availableLetters)
        {
            _availableLetters = availableLetters;
        }
        public LetterDTO GenerateRandomLetter()
        {
            Random rnd = new Random();
            return new LetterDTO(_availableLetters[rnd.Next(0, _availableLetters.Length)]);
        }
    }
}
