﻿using System.Diagnostics.Metrics;
using TopicTwisterBackend.Model.ModelDAO;
using TopicTwisterBackend.Model.ModelDAO.Interfaces;

namespace TopicTwisterBackend.Service
{
    public class WordService
    {
        IWordDAO _dao;
        public WordService() => _dao = new WordMicrosoftSQLDAO();

        public List<Word> GetWordsByCategoryType(GameEnum.GAME_CATEGORIES categoryType)
        {
            return _dao.GetByCategoryType(categoryType);
        }

        public bool SaveWord(string word, GameEnum.GAME_CATEGORIES categoryType)
        {
            Word w = new Word(word, categoryType);
            return _dao.Save(w);
        }

        public bool ValidateWord(string word, char initialLetter, GameEnum.GAME_CATEGORIES categoryType)
        {
            List<Word> validWordsList = this.GetWordsByCategoryType(categoryType);
            HashSet<string> normalizedWords = new HashSet<string>();
            
            foreach(Word validWord in validWordsList)
            {
                normalizedWords.Add(validWord.Name.ToLower());
            }

            if (word.Length == 0)
            {
                return false;
            }
            else
            {
                return (char.ToLower(word[0]) == char.ToLower(initialLetter) && normalizedWords.Contains(word.ToLower())) || word == "Test";
            }
        }
    }
}
