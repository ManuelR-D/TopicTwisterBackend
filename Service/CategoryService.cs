﻿using TopicTwisterBackend.Generators;
using TopicTwisterBackend.Model.ModelDAO;
using TopicTwisterBackend.Model.ModelDAO.Interfaces;
using TopicTwisterBackend.Model.ModelDTO;

namespace TopicTwisterBackend.Service
{
    public class CategoryService
    {
        private ICategoryDAO _dao;

        public CategoryService() => _dao = new CategoryMicrosoftSQLDAO();
        public CategoryService(ICategoryDAO dao) => _dao = dao;

        public List<CategoryDTO> GetAllCategories()
        {
            List<CategoryDTO> result = new();

            List<Category> categories = _dao.GetAllCategories();
            foreach (Category cat in categories)
            {
                result.Add(new CategoryDTO(cat.CategoryName, new List<WordDTO>()));
            }
            return result;
        }

        public List<CategoryDTO> GetRandomCategories(int numberOfCategories)
        {
            RandomCategory randomCategoryGenerator = new RandomCategory(this.GetAllCategories());
            return randomCategoryGenerator.GetRandomCategories(numberOfCategories);
        }

    }
}
