﻿using java.nio.charset;
using System.Security.Cryptography;
using System.Text;
using TopicTwisterBackend.Model;
using TopicTwisterBackend.Model.ModelDAO;
using TopicTwisterBackend.Model.ModelDAO.Interfaces;

namespace TopicTwisterBackend.Service
{
    public class PlayerService
    {
        private PlayerDAO _dao;
        public PlayerService() { _dao = new PlayerMicrosoftSQLDAO(); }

        public Player Get(string username, string password)
        {
            string passwordHash = sha256_hash(password);
            return _dao.Get(username, passwordHash);
        }

        //https://stackoverflow.com/questions/16999361/obtain-sha-256-string-of-a-string
        public static string sha256_hash(string value)
        {
            StringBuilder Sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }
    }
}
