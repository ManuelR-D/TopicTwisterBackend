using TopicTwisterBackend.Model;

[Serializable]
public class Round 
{
    public int GameId;
    public List<Turn> Turns;
    public int RoundNumber;
    private GameEnum.ROUND_STATE RoundState;

    public Round(int gameId, int roundNumber, Player playerOne, Player playerTwo)
    {
        GameId = gameId;
        Turns = new List<Turn>() 
        {
            new Turn(1, roundNumber, GameId, playerOne),
            new Turn(2, roundNumber, GameId, playerTwo)
        };
        RoundNumber = roundNumber;
        RoundState = GameEnum.ROUND_STATE.WAITING;
    }

    public void StartRound()
    {
        this.RoundState = GameEnum.ROUND_STATE.IN_PROGRESS;
        Turns[0].TurnState = GameEnum.TURN_STATE.IN_PROGRESS;
    }
    public void EndRound()
    {
        this.RoundState = GameEnum.ROUND_STATE.FINISHED;
        Turns.ForEach(t => t.TurnState = GameEnum.TURN_STATE.FINISHED);
    }

    public Turn GetTurnInProgress()
    {
        foreach(Turn t in Turns)
        {
            if(t.TurnState == GameEnum.TURN_STATE.IN_PROGRESS)
            {
                return t;
            }
        }
        return null;
    }

    //Finish turn in progress and set next turn to in progress
    public void FinishActualTurn()
    {
        GetTurnInProgress().TurnState = GameEnum.TURN_STATE.FINISHED;
        foreach(Turn t in Turns)
        {
            if(t.TurnState == GameEnum.TURN_STATE.WAITING)
            {
                t.TurnState = GameEnum.TURN_STATE.IN_PROGRESS;
                return;
            }
        }
    }

    public GameEnum.ROUND_STATE GetRoundState()
    {
        return this.RoundState;
    }

    public string GetWinner()
    {
        if(RoundState != GameEnum.ROUND_STATE.FINISHED)
        {
            return null;
        }
        int playerOnePoints = Turns[0].GetPoints();
        int playerTwopoints = Turns[1].GetPoints();
        if (playerOnePoints == playerTwopoints)
        {
            return "Tie";
        } 
        else if (playerOnePoints > playerTwopoints)
        {
            return Turns[0].playerTurn.Name;
        }
        else
        {
            return Turns[1].playerTurn.Name;
        }
    }

    public override bool Equals(object obj)
    {
        Round r2 = obj as Round;
        foreach(Turn t in Turns)
        {
            if (!r2.Turns.Contains(t))
            {
                return false;
            }
        }
        return obj is Round round &&
               GameId == round.GameId &&
               RoundNumber == round.RoundNumber &&
               RoundState == round.RoundState;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(GameId, Turns, RoundNumber, RoundState);
    }
}
