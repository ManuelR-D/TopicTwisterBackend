[Serializable]
public class Word 
{
    public string Name;
    public GameEnum.GAME_CATEGORIES Category;

    public Word(string name, GameEnum.GAME_CATEGORIES category)
    {
        Name = name;
        Category = category;
    }

    public override bool Equals(object obj)
    {
        Word word2 = obj as Word;
        return obj is Word word &&
               Name == word.Name &&
               Category == word.Category;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(Name, Category);
    }

    public override string ToString()
    {
        return this.Name;
    }

}
