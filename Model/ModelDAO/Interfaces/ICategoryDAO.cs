﻿namespace TopicTwisterBackend.Model.ModelDAO.Interfaces
{
    public interface ICategoryDAO
    {
        Category GetCategoryByName(string categoryName);
        void SaveCategory(string categoryName);
        List<Category> GetAllCategories();
    }
}
