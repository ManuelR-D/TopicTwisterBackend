﻿namespace TopicTwisterBackend.Model.ModelDAO.Interfaces
{
    public interface PlayerDAO
    {
        public Player Get(string username, string passwordHash);
    }
}
