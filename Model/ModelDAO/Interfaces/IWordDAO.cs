﻿namespace TopicTwisterBackend.Model.ModelDAO.Interfaces
{
    public interface IWordDAO
    {
        List<Word> GetByCategoryType(GameEnum.GAME_CATEGORIES categoryType);
        public List<Word> GetAllWords();
        public bool Save(Word word);
    }
}
