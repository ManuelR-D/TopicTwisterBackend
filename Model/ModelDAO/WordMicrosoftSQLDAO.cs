﻿using System.Data.SqlClient;
using TopicTwisterBackend.Model.ModelDAO.Interfaces;

namespace TopicTwisterBackend.Model.ModelDAO
{
    public class WordMicrosoftSQLDAO : IWordDAO
    {
        private string _connectionString;
        private const string TABLE_NAME = "word";
        public WordMicrosoftSQLDAO(string connectionString) => _connectionString = connectionString;
        public WordMicrosoftSQLDAO() 
        {
            _connectionString = GameConfig.DefaultConnectionString;
        }
        public List<Word> GetAllWords()
        {
            List<Word> words = new List<Word>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String query = $"SELECT * FROM {TABLE_NAME} INNER JOIN category ON word.categoryId = category.id";
                SqlCommand cmd = new SqlCommand(query, connection);
                SqlDataReader registers = cmd.ExecuteReader();
                while(registers.Read()) 
                {
                    GameEnum.GAME_CATEGORIES category = (GameEnum.GAME_CATEGORIES)Int32.Parse(registers["categoryId"].ToString());
                    words.Add(new Word(registers["wordName"].ToString(), category));
                }
            }
            return words;
        }

        public List<Word> GetByCategoryType(GameEnum.GAME_CATEGORIES categoryType)
        {
            List<Word> words = new List<Word>();
            int categoryId = (int)categoryType;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String query = $"SELECT * FROM {TABLE_NAME} INNER JOIN category ON word.categoryId = category.id WHERE category.id = {categoryId}";
                SqlCommand cmd = new SqlCommand(query, connection);
                SqlDataReader registers = cmd.ExecuteReader();
                while (registers.Read())
                {
                    GameEnum.GAME_CATEGORIES category = (GameEnum.GAME_CATEGORIES)Int32.Parse(registers["categoryId"].ToString());
                    words.Add(new Word(registers["wordName"].ToString(), category));
                }
            }
            return words;
        }

        public bool Save(Word w)
        {
            int categoryId = (int)w.Category;
            int rowsAffected = 0;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String query = $"INSERT INTO {TABLE_NAME}(wordName,categoryId) VALUES ('{w.Name}',{categoryId})";
                SqlCommand cmd = new SqlCommand(query, connection);
                rowsAffected = cmd.ExecuteNonQuery();
            }
            return rowsAffected > 0 ? true : false;
        }
    }
}
