﻿using System.Data.SqlClient;
using TopicTwisterBackend.Model.ModelDAO.Interfaces;
using TopicTwisterBackend.Model.ModelDTO;
namespace TopicTwisterBackend.Model.ModelDAO
{
    public class PlayerMicrosoftSQLDAO : PlayerDAO
    {
        private string _connectionString;
        private const string TABLE_NAME = "player";
        public PlayerMicrosoftSQLDAO(string connectionString) => _connectionString = connectionString;
        public PlayerMicrosoftSQLDAO()
        {
            _connectionString = GameConfig.DefaultConnectionString;
        }
        public Player Get(string username, string passwordHash)
        {
            Player p = null;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String query = $"SELECT * FROM {TABLE_NAME} WHERE username = '{username}' AND password = '{passwordHash}'";
                SqlCommand cmd = new SqlCommand(query, connection);
                SqlDataReader registers = cmd.ExecuteReader();
                while (registers.Read())
                {
                    string playerUsername = registers["username"].ToString();
                    string email = registers["email"].ToString();
                    p = new Player(playerUsername, email, new List<GameDTO>());
                }
            }
            return p;
        }
    }
}
