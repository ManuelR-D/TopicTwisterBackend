﻿using System.Data.SqlClient;
using TopicTwisterBackend.Model.ModelDAO.Interfaces;

namespace TopicTwisterBackend.Model.ModelDAO
{
    public class CategoryMicrosoftSQLDAO : ICategoryDAO
    {
        private string _connectionString;
        private const string TABLE_NAME = "Category";
        public CategoryMicrosoftSQLDAO(string connectionString) => _connectionString = connectionString;
        public CategoryMicrosoftSQLDAO() 
        {
            _connectionString = GameConfig.DefaultConnectionString;
        }
        public List<Category> GetAllCategories()
        {
            List<Category> categories = new List<Category>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                String query = $"SELECT * FROM {TABLE_NAME}";
                SqlCommand cmd = new SqlCommand(query, connection);
                SqlDataReader registers = cmd.ExecuteReader();
                while(registers.Read()) 
                {
                    categories.Add(new Category(registers["categoryType"].ToString()));
                }
            }
            return categories;
        }

        public Category GetCategoryByName(string categoryName)
        {
            throw new NotImplementedException();
        }

        public void SaveCategory(string categoryName)
        {
            throw new NotImplementedException();
        }
    }
}
