﻿using System;

[Serializable]
public class GameEnum
{
    public enum GAME_STATE
    {
        IN_PROGRESS,
        FINISHED
    }
    public enum ROUND_STATE
    {
        WAITING,
        IN_PROGRESS,
        FINISHED
    }
    public enum TURN_STATE
    {
        WAITING,
        IN_PROGRESS,
        FINISHED
    }
    public enum GAME_CATEGORIES:int
    {
        Artists = 1,
        Sports = 2,
        Books = 3,
        Countries = 4,
        Movies = 5,
        Videogames = 6, 
        Sciencetists = 7,
        Random = 8
    }
}
