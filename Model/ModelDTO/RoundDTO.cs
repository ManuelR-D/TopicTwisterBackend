﻿namespace TopicTwisterBackend.Model.ModelDTO
{
    public class RoundDTO
    {
        public int GameId { get; set; }
        public List<TurnDTO> Turns { get; set; }
        public int RoundNumber { get; set; }
        private GameEnum.ROUND_STATE RoundState { get; set; }

        public RoundDTO(int gameId, List<TurnDTO> turns, int roundNumber, GameEnum.ROUND_STATE roundState)
        {
            GameId = gameId;
            Turns = turns;
            RoundNumber = roundNumber;
            RoundState = roundState;
        }
    }
}
