﻿namespace TopicTwisterBackend.Model.ModelDTO
{
    public class TurnDTO
    {
        public int Id { get; set; }
        public int RoundId { get; set; }
        public int GameId { get; set; }
        public GameEnum.TURN_STATE TurnState { get; set; }
        short MaxTime { get; set; }
        public LetterDTO Letter { get; set; }
        public List<CategoryDTO> categories { get; set; }
        public List<WordDTO> userWords { get; set; }
        public PlayerDTO playerTurn { get; set; }

        public TurnDTO(int id, int roundId, int gameId, GameEnum.TURN_STATE turnState, short maxTime, LetterDTO letter, List<CategoryDTO> categories, List<WordDTO> userWords, PlayerDTO playerTurn)
        {
            Id = id;
            RoundId = roundId;
            GameId = gameId;
            TurnState = turnState;
            MaxTime = maxTime;
            Letter = letter;
            this.categories = categories;
            this.userWords = userWords;
            this.playerTurn = playerTurn;
        }
    }
}
