namespace TopicTwisterBackend.Model.ModelDTO
{
    [Serializable]
    public class PlayerDTO 
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public List<GameDTO> HistoryGames { get; set; }

        public PlayerDTO(string name, string email, List<GameDTO> historyGames)
        {
            Name = name;
            Email = email;
            HistoryGames = historyGames;
        }
    }
}
