﻿namespace TopicTwisterBackend.Model.ModelDTO
{
    public class GameDTO
    {
        public int Id { get; set; }
        public GameEnum.GAME_STATE GameState { get; set; }
        public List<PlayerDTO> Players { get; set; }
        public List<RoundDTO> Rounds { get; set; }

        public GameDTO(int id, GameEnum.GAME_STATE gameState, List<PlayerDTO> players, List<RoundDTO> rounds)
        {
            Id = id;
            GameState = gameState;
            Players = players;
            Rounds = rounds;
        }
    }
}
