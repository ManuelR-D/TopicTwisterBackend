namespace TopicTwisterBackend.Model.ModelDTO
{
    [Serializable]
    public class CategoryDTO
    {
        public string CategoryName { get; set; }
        public List<WordDTO> WordList { get; set; }

        public CategoryDTO(string categoryName, List<WordDTO> wordList)
        {
            CategoryName = categoryName;
            WordList = wordList;
        }
    }
}

