﻿namespace TopicTwisterBackend.Model.ModelDTO
{
    public class LetterDTO
    {
        public char Letter { get; set; }

        public LetterDTO(char letter)
        {
            Letter = letter;
        }
    }
}
