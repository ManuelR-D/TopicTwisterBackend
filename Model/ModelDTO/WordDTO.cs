﻿namespace TopicTwisterBackend.Model.ModelDTO
{
    [Serializable]
    public class WordDTO
    {
        public string Name { get; set; }
        public GameEnum.GAME_CATEGORIES Category { get; set; }

        public WordDTO(string name, GameEnum.GAME_CATEGORIES category)
        {
            Name = name;
            Category = category;
        }
    }
}
