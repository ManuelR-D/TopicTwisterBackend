﻿namespace TopicTwisterBackend.Model
{
    public class GameConfig
    {
        public static readonly short TurnTime = 60;
        public static readonly string DefaultConnectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=TopicTwister;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
    }
}
