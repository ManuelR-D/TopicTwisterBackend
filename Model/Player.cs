﻿using TopicTwisterBackend.Model.ModelDTO;

namespace TopicTwisterBackend.Model
{
    public class Player : PlayerDTO
    {
        public Player(string name, string email, List<GameDTO> historyGames) : base(name, email, historyGames)
        {
        }
    }
}
