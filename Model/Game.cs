using TopicTwisterBackend.Model;

[Serializable]
public class Game 
{
    public int id;
    public GameEnum.GAME_STATE GameState;
    public List<Player> Players;
    public List<Round> Rounds;

    public Game(List<Player> players)
    {
        this.id = 0;
        GameState = GameEnum.GAME_STATE.IN_PROGRESS;
        Players = players;
        Rounds = new List<Round>()
        {
            new Round(id, 1, players[0], players[1]),
            new Round(id, 2, players[0], players[1]),
            new Round(id, 3, players[0], players[1])
        };
        Rounds[0].StartRound();
    }

    public override bool Equals(object obj)
    {
        Game game2 = obj as Game;
        foreach(Player p in Players)
        {
            if (!game2.Players.Contains(p))
            {
                return false;
            }
        }
        foreach(Round r in Rounds)
        {
            if (!game2.Rounds.Contains(r))
            {
                return false;
            }
        }
        return obj is Game game &&
               id == game.id &&
               GameState == game.GameState;
    }

    public void FinishActualRound()
    {
        GetInProgressRound().EndRound();
        foreach(Round r in Rounds)
        {
            if(r.GetRoundState() == GameEnum.ROUND_STATE.WAITING)
            {
                r.StartRound();
                return;
            }
        }
        //All rounds finished, finish game
        GameState = GameEnum.GAME_STATE.FINISHED;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(id, GameState, Players, Rounds);
    }

    public Round GetInProgressRound()
    {
        foreach(Round r in Rounds)
        {
            if(r.GetRoundState() == GameEnum.ROUND_STATE.IN_PROGRESS)
            {
                return r;
            }
        }
        return null;
    }

    public Round GetLastFinishedRound()
    {
        for(int i = Rounds.Count - 1; i >= 0; i--)
        {
            Round r = Rounds[i];
            if(r.GetRoundState() == GameEnum.ROUND_STATE.FINISHED)
            {
                return r;
            }
        }
        return null;
    }

    public string GetWinner()
    {
        Dictionary<string,int> scoreboard = new Dictionary<string, int>() 
        {
            { Players[0].Name,0 },
            { Players[1].Name,0 }
        };
        foreach(Round r in Rounds)
        {
            string winner = r.GetWinner();
            if(winner == "Tie")
            {
                scoreboard[Players[0].Name]++;
                scoreboard[Players[1].Name]++;
            } 
            else
            {
                scoreboard[r.GetWinner()]++;
            }
        }
        string winnerName;
        if (scoreboard[Players[0].Name] == scoreboard[Players[1].Name])
        {
            winnerName = "Tie";
        } else
        {
            winnerName = scoreboard[Players[0].Name] > scoreboard[Players[1].Name] ? Players[0].Name : Players[1].Name;
        }
        return winnerName;
    }
}
