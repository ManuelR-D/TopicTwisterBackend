[Serializable]
public class Letter {
    public char letter;

    public Letter(char letter)
    {
        this.letter = letter;
    }

    public override bool Equals(object obj)
    {
        return obj is Letter letter &&
               this.letter == letter.letter;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(letter);
    }
}
