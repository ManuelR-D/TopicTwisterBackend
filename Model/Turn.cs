using TopicTwisterBackend.Generators;
using TopicTwisterBackend.Model;
using TopicTwisterBackend.Model.ModelDTO;
using TopicTwisterBackend.Validators;

[Serializable]
public class Turn
{
    public int Id;
    public int RoundId;
    public int GameId;
    public GameEnum.TURN_STATE TurnState;
    short MaxTime;
    public Letter Letter;
    public List<Category> categories;
    public List<Word> userWords;
    public Player playerTurn;


    public Turn(int id, int roundId, int gameId, Player playerTurn)
    {
        Id = id;
        RoundId = roundId;
        GameId = gameId;
        MaxTime = GameConfig.TurnTime;
        Letter = new Letter(new RandomLetter().GenerateRandomLetter().Letter);
        categories = new List<Category>(5);
        userWords = new List<Word>(5);
        List<CategoryDTO> randomCategories = new RandomCategory().GetRandomCategories(5);
        foreach(CategoryDTO cat in randomCategories) 
        {
            Category catEntity = new Category(cat);
            categories.Add(catEntity);
            userWords.Add(new Word("", catEntity.CategoryType));
        }
        this.playerTurn = playerTurn;
        this.TurnState = GameEnum.TURN_STATE.WAITING;
    }

    public int GetPoints()
    {
        WordValidator validator = new WordValidator();
        int score = 0;
        foreach(Word w in userWords)
        {
            Category categoryEntity = new Category(w.Category);
            if (validator.Validate(w.Name, categoryEntity.ToString(), Letter.letter))
            {
                score++;
            }
        }
        return score;
        
    }

    public override bool Equals(object obj)
    {
        Turn turn2 = (Turn)obj;
        foreach (Category cat in categories)
        {
            if (!turn2.categories.Contains(cat))
            {
                return false;
            }
        }
        foreach (Word word in userWords)
        {
            if (!turn2.userWords.Contains(word))
            {
                return false;
            }
        }
        return obj is Turn turn &&
               Id == turn.Id &&
               RoundId == turn.RoundId &&
               GameId == turn.GameId &&
               TurnState == turn.TurnState &&
               MaxTime == turn.MaxTime &&
               Letter.Equals(turn.Letter) &&
               playerTurn.Equals(turn.playerTurn);
    }

    public override int GetHashCode()
    {
        HashCode hash = new HashCode();
        hash.Add(Id);
        hash.Add(RoundId);
        hash.Add(GameId);
        hash.Add(TurnState);
        hash.Add(MaxTime);
        hash.Add(Letter);
        hash.Add(categories);
        hash.Add(userWords);
        hash.Add(playerTurn);
        return hash.ToHashCode();
    }
}
