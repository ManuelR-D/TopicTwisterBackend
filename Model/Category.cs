using TopicTwisterBackend.Controllers;
using TopicTwisterBackend.Model.ModelDTO;
using TopicTwisterBackend.Service;

[Serializable]
public class Category
{
    public GameEnum.GAME_CATEGORIES CategoryType;
    public string CategoryName;
    public List<Word> WordList => new WordService().GetWordsByCategoryType(CategoryType);

    private readonly Dictionary<string, GameEnum.GAME_CATEGORIES> CategoryNameToEnum = new()
    {
        { "Artists", GameEnum.GAME_CATEGORIES.Artists},
        { "Countries", GameEnum.GAME_CATEGORIES.Countries},
        { "Videogames", GameEnum.GAME_CATEGORIES.Videogames},
        { "Books", GameEnum.GAME_CATEGORIES.Books},
        { "Movies", GameEnum.GAME_CATEGORIES.Movies},
        { "Random", GameEnum.GAME_CATEGORIES.Random},
        { "Sciencetists", GameEnum.GAME_CATEGORIES.Sciencetists},
        { "Sports", GameEnum.GAME_CATEGORIES.Sports},
    };

    private readonly Dictionary<GameEnum.GAME_CATEGORIES, string> EnumToCategoryName = new()
    {
        { GameEnum.GAME_CATEGORIES.Artists, "Artists"},
        { GameEnum.GAME_CATEGORIES.Countries, "Countries" },
        { GameEnum.GAME_CATEGORIES.Videogames,"Videogames" },
        { GameEnum.GAME_CATEGORIES.Books, "Books"},
        { GameEnum.GAME_CATEGORIES.Movies,"Movies"},
        { GameEnum.GAME_CATEGORIES.Random,"Random"},
        { GameEnum.GAME_CATEGORIES.Sciencetists, "Sciencetists"},
        { GameEnum.GAME_CATEGORIES.Sports,"Sports"},
    };

    public Category(GameEnum.GAME_CATEGORIES categoryType)
    {
        CategoryType = categoryType;
        CategoryName = EnumToCategoryName[categoryType];
    }

    public Category(CategoryDTO category)
    {
        CategoryType = CategoryNameToEnum[category.CategoryName];
        CategoryName = category.CategoryName;
    }

    public Category(string categoryTitle)
    {
        CategoryType = CategoryNameToEnum[categoryTitle];
        CategoryName = categoryTitle;
    }

    public override string ToString()
    {
        return EnumToCategoryName[this.CategoryType];
    }

    public override bool Equals(object obj)
    {
        Category cat2 = obj as Category;
        foreach(Word w in this.WordList)
        {
            if(!cat2.WordList.Contains(w))
            {
                return false;
            }
        }
        return obj is Category category &&
               CategoryType == category.CategoryType;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(CategoryType, WordList, CategoryNameToEnum, EnumToCategoryName);
    }
}
