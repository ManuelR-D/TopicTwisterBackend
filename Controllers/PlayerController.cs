using Microsoft.AspNetCore.Mvc;
using TopicTwisterBackend.Model.ModelDTO;
using TopicTwisterBackend.Service;

namespace TopicTwisterBackend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PlayerController : ControllerBase
    {

        private PlayerService _service = new PlayerService();
        private readonly ILogger<PlayerController> _logger;

        public PlayerController(ILogger<PlayerController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "Get")]
        public PlayerDTO Get(string name, string password)
        {
            return _service.Get(name, password);
        }

        [HttpPost(Name = "Save")]
        public bool Save(string name, string password)
        {
            //TODO:
            return true;
        }
    }
}