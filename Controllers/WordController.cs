using Microsoft.AspNetCore.Mvc;
using TopicTwisterBackend.Model.ModelDTO;
using TopicTwisterBackend.Service;

namespace TopicTwisterBackend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WordController : ControllerBase
    {

        private readonly ILogger<WordController> _logger;
        private WordService _service;
        private static readonly Dictionary<string, GameEnum.GAME_CATEGORIES> _gameCategories = new Dictionary<string, GameEnum.GAME_CATEGORIES>()
        {
            {"artists", GameEnum.GAME_CATEGORIES.Artists },
            {"sports", GameEnum.GAME_CATEGORIES.Sports },
            {"books", GameEnum.GAME_CATEGORIES.Books },
            {"countries", GameEnum.GAME_CATEGORIES.Countries },
            {"movies", GameEnum.GAME_CATEGORIES.Movies },
            {"videogames", GameEnum.GAME_CATEGORIES.Videogames },
            {"sciencetists", GameEnum.GAME_CATEGORIES.Sciencetists },
            {"random", GameEnum.GAME_CATEGORIES.Random },
        };

        public WordController(ILogger<WordController> logger)
        {
            _logger = logger;
            _service = new WordService();
        }

        //[HttpGet(Name = "GetPlayer")]
        //public Player Get(string name)
        //{
        //    return new Player(name);
        //}

        [HttpGet("IsValidWord")]
        public bool IsValidWord(string word, char initialLetter, string categoryType)
        {
            if (!_gameCategories.ContainsKey(categoryType.ToLower()))
            {
                return false;
            }
            bool result = _service.ValidateWord(word, initialLetter, _gameCategories[categoryType.ToLower()]);
            Console.WriteLine($"{word};{initialLetter};{categoryType}->" + result);
            return result;
        }

        [HttpGet("GetByCategoryType")]
        public IEnumerable<WordDTO> GetByCategoryType(string categoryType) 
        {
            if (!_gameCategories.ContainsKey(categoryType.ToLower()))
            {
                return null;
            }
            List<WordDTO> result = new();
            List<Word> words = _service.GetWordsByCategoryType(_gameCategories[categoryType.ToLower()]);
            foreach (Word word in words)
            {
                result.Add(new WordDTO(word.Name, word.Category));
            }
            return result;
        }
    }
}