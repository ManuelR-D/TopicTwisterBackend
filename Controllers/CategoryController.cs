using Microsoft.AspNetCore.Mvc;
using TopicTwisterBackend.Generators;
using TopicTwisterBackend.Model;
using TopicTwisterBackend.Model.ModelDAO;
using TopicTwisterBackend.Model.ModelDAO.Interfaces;
using TopicTwisterBackend.Model.ModelDTO;
using TopicTwisterBackend.Service;

namespace TopicTwisterBackend.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {

        private readonly ILogger<CategoryController> _logger;
        private ICategoryDAO _dao;
        private CategoryService _service;

        public CategoryController(ILogger<CategoryController> logger)
        {
            _logger = logger;
            _service = new CategoryService();
        }

        [HttpGet("GetAllCategories")]
        public IEnumerable<CategoryDTO> GetAllCategories()
        {
            return _service.GetAllCategories();
        }

        [HttpGet("GetRandomCategories")]
        public IEnumerable<CategoryDTO> GetRandomCategories(int numberOfCategories)
        {
            return _service.GetRandomCategories(numberOfCategories);
        }

        //[HttpGet(Name = "GetCategoryByType")]
        //public CategoryDTO GetCategoryByType(string categoryType)
        //{
        //Category category = _dao.GetCategoryByType(); //TODO
        //return new CategoryDTO(category.CategoryName, new List<WordDTO>());
        //}
    }
}