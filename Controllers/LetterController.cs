using Microsoft.AspNetCore.Mvc;
using TopicTwisterBackend.Generators;
using TopicTwisterBackend.Model;
using TopicTwisterBackend.Model.ModelDTO;

namespace TopicTwisterBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LetterController : ControllerBase
    {

        private readonly ILogger<LetterController> _logger;

        public LetterController(ILogger<LetterController> logger)
        {
            _logger = logger;
        }

        [HttpGet(Name = "GetRandomLetter")]
        public LetterDTO GetRandomLetter()
        {
            RandomLetter randomLetterGenerator = new RandomLetter();
            return new LetterDTO(randomLetterGenerator.GenerateRandomLetter().Letter);
        }
    }
}