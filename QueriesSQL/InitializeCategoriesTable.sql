﻿use TopicTwister;

DELETE FROM Category;

INSERT INTO Category(CategoryName) VALUES ('Artists');
INSERT INTO Category(CategoryName) VALUES ('Sports');
INSERT INTO Category(CategoryName) VALUES ('Books');
INSERT INTO Category(CategoryName) VALUES ('Countries');
INSERT INTO Category(CategoryName) VALUES ('Movies');
INSERT INTO Category(CategoryName) VALUES ('Videogames');
INSERT INTO Category(CategoryName) VALUES ('Sciencetists');
INSERT INTO Category(CategoryName) VALUES ('Random');

SELECT * FROM Category