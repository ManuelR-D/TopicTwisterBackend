﻿CREATE TABLE [dbo].[Table]
(
	[id] INT NOT NULL PRIMARY KEY, 
    [wordName] VARBINARY(50) NOT NULL, 
    [categoryId] INT NOT NULL, 
    CONSTRAINT [FK_category_word] FOREIGN KEY ([categoryId]) REFERENCES [category]([id])
)
